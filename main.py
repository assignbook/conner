import requests

webhook_url = "https://discordapp.com/api/webhooks/1009935157970612366/SDsM-4MpSJRJv92fg3SUXhy4ARLdUn24YMiFn7VqMAMKZJOYyMfdTF4UwEON6Juc7pFS"
session = requests.Session()
assignment_input = input("What is the Assignment Called?\n>>")
subject_input = input("What is the Subject of this Assignment\n>>")
description = f"{assignment_input} (in {subject_input})"

while True:
    data = {
  "content": "||@everyone||",
  "embeds": [
    {
      "title": "New Assigment",
      "description": description,
      "color": 16777215,
      "thumbnail": {
        "url": "https://www.startpage.com/av/proxy-image?piurl=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F4%2F42%2FNotebook_and_Pen_Flat_Icon_Vector.svg%2F1024px-Notebook_and_Pen_Flat_Icon_Vector.svg.png&sp=1660857828T3ac2b90bb59c45023cd17d173cdd3b5d26ffb400f14b1ee7ed57065e11ac328e"
      }
    }
  ],
  "username": "Assignment Notebook",
  "avatar_url": "https://www.startpage.com/av/proxy-image?piurl=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F4%2F42%2FNotebook_and_Pen_Flat_Icon_Vector.svg%2F1024px-Notebook_and_Pen_Flat_Icon_Vector.svg.png&sp=1660857828T3ac2b90bb59c45023cd17d173cdd3b5d26ffb400f14b1ee7ed57065e11ac328e",
  "attachments": []
}

    r = session.post(webhook_url, json=data)

    assignment_input = input("What is the Next Assignment Called?\n>>")
    subject_input = input("What is the Subject of this Assignment\n>>")